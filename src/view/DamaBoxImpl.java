package view;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import model.CheckerBoardShadow.PiecesType;
import java.awt.*;
import java.io.IOException;
//import java.io.InputStream;
import java.net.URL;


/**
 * implementation of {@link DamaBox} interface.
 */
public class DamaBoxImpl extends JButton implements DamaBox {


    private static final long serialVersionUID = -1359251019723175129L;
    /**
     * Constructor
     */
    public DamaBoxImpl() 
    {
        super();
    }



    public void setPiece(PiecesType type) throws  IOException 
    {

        if(type==PiecesType.WP)
        {
            final URL imgURL = DamaBoxImpl.class.getResource("/img/whitePiece.png");
            final ImageIcon icon = new ImageIcon(imgURL);
            resize(icon);


        }
        else if(type==PiecesType.BP){

            final URL imgURL = DamaBoxImpl.class.getResource("/img/blackPiece.png");
            final ImageIcon icon = new ImageIcon(imgURL);
            resize(icon);

        }
        else if(type==PiecesType.WD){
            final URL imgURL = DamaBoxImpl.class.getResource("/img/whiteDama.png");
            final ImageIcon icon = new ImageIcon(imgURL);
            resize(icon);


        }
        else if(type==PiecesType.BD){

            final URL imgURL = DamaBoxImpl.class.getResource("/img/blackDama.png");
            final ImageIcon icon = new ImageIcon(imgURL);
            resize(icon);

        }else {
            this.setIcon(null);
        }



    }

    public void resize(ImageIcon icon) 
    {

        Image img = icon.getImage();
        Image newimg = img.getScaledInstance(100, 100,  java.awt.Image.SCALE_SMOOTH);
        this.setIcon(new ImageIcon(newimg));
    }

}
